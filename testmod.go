package testmod

import "fmt"

type Person struct {
	Name      string
	SureNaame string
	Address   address
}

type address struct {
	Contry      string
	City        string
	Street      string
	HomeNumber  uint
	PostalcCode uint
}

func MewPerson(name, surename string, country, city, street string, homeNumber, postalCode uint) Person {
	p := Person{
		Name:      name,
		SureNaame: surename,
		Address: address{
			Contry:      country,
			City:        city,
			Street:      street,
			HomeNumber:  homeNumber,
			PostalcCode: postalCode,
		},
	}
	return p
}

func (p *Person) getPersonInfo() {
	fmt.Printf("%v", p)
}
